package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MatchOutfitBodyActivity extends AppCompatActivity {

    private String strGender, strCategory, strTop, strBottom, strShoes;
    private boolean blnTop, blnBottom, blnShoes;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1) {
            if(resultCode == RESULT_OK) {
                checkBodyPart(data.getStringExtra("BODY_PART"), data.getStringExtra("MESSAGE"));
            } else if(resultCode == RESULT_CANCELED) {
                Log.e("PASSED_DATA", "Cancelled");
            }
                else {
                Log.e("PASSED_DATA", "SHIT");
            }
        }
    }

    private void checkBodyPart(String strBodyPart, String strSelectedOutfit) {
        switch(strBodyPart) {
            case "Top":
                blnTop = true;
                strTop = strSelectedOutfit;
                break;
            case "Bottom":
                blnBottom = true;
                strBottom = strSelectedOutfit;
                break;
            case "Shoes":
                blnShoes = true;
                strShoes = strSelectedOutfit;
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_outfit_body);
        setUpData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpData() {
        Bundle extraData = getIntent().getExtras();

        if(extraData != null) {
            strGender = extraData.getString("GENDER");
            strCategory = extraData.getString("CATEGORY");
        }

        setTitle("Match Outfit Parts");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void topOnClick(View view) {
        setExtraData("Top");
    }

    public void bottomOnClick(View view) {
        setExtraData("Bottom");
    }

    public void shoesOnClick(View view) {
        setExtraData("Shoes");
    }

    public void nextOnClick(View view) {
        if(!blnTop || !blnBottom || !blnShoes) {
            Toast.makeText(MatchOutfitBodyActivity.this, "Unable to proceed. (Incomplete Outfit " +
                    "Selection)",
                    Toast.LENGTH_SHORT)
                    .show();
        } else {
            Intent intentOutfitView =
                    new Intent(MatchOutfitBodyActivity.this, MatchOutfitView.class);

            intentOutfitView.putExtra("CATEGORY", strCategory);
            intentOutfitView.putExtra("GENDER", strGender);
            intentOutfitView.putExtra("TOP", strTop);
            intentOutfitView.putExtra("BOTTOM", strBottom);
            intentOutfitView.putExtra("SHOES", strShoes);

            startActivity(intentOutfitView);
        }
    }

    private void setExtraData(String strBody) {
        Intent intentTop = new Intent(MatchOutfitBodyActivity.this, BodyActivity.class);

        intentTop.putExtra("GENDER", strGender);
        intentTop.putExtra("CATEGORY", strCategory);
        intentTop.putExtra("BODY", strBody);

        startActivityForResult(intentTop, 1);
    }
}
