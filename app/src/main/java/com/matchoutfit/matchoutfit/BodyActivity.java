package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class BodyActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String strGender, strCategory, strBody, strBodyPart;
    private ArrayAdapter<CharSequence> adapterTop;
    private ImageView imgViewTop;
    private Spinner spinnerTop;

    @Override
    public void onBackPressed() {
        Intent intentOutfit = new Intent();
        intentOutfit.putExtra("MESSAGE", spinnerTop.getSelectedItem().toString());
        intentOutfit.putExtra("BODY_PART", strBodyPart);

        Toast.makeText(BodyActivity.this, strBodyPart + " Saved", Toast.LENGTH_SHORT).show();

        setResult(RESULT_OK, intentOutfit);

        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body);
        setUpData();
        setUpSpinnerData();
        setActionBarTitle();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setActionBarTitle() {
        setTitle(String.format("%s %s (%s)", strGender, strBodyPart, strCategory));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpData() {
        Bundle extraData = getIntent().getExtras();

        if (extraData != null) {
            strGender = extraData.getString("GENDER");
            strCategory = extraData.getString("CATEGORY");
            strBody = extraData.getString("BODY");
        }

        imgViewTop = (ImageView) findViewById(R.id.img_view_top);
    }

    private void setUpSpinnerData() {
        spinnerTop = (Spinner) findViewById(R.id.spinner_tops);

        if (strGender.equals("Female")) {
            switch (strBody) {
                case "Top":
                    setUpAdapterData(strCategory.equals("Casual")
                            ? R.array.female_casuals_top_array
                            : R.array.female_formals_top_array);
                    strBodyPart = "Top";
                    break;
                case "Bottom":
                    setUpAdapterData(strCategory.equals("Casual")
                            ? R.array.female_casuals_bottom_array
                            : R.array.female_formals_bottom_array);
                    strBodyPart = "Bottom";
                    break;
                case "Shoes":
                    setUpAdapterData(strCategory.equals("Casual")
                            ? R.array.female_casuals_shoes_array
                            : R.array.female_formals_shoes_array);
                    strBodyPart = "Shoes";
                    break;
            }
        } else {    // Male
            switch (strBody) {
                case "Top":
                    setUpAdapterData(strCategory.equals("Casual")
                            ? R.array.male_casuals_top_array
                            : R.array.male_formals_top_array);
                    strBodyPart = "Top";
                    break;
                case "Bottom":
                    setUpAdapterData(strCategory.equals("Casual")
                            ? R.array.male_casuals_bottom_array
                            : R.array.male_formals_bottom_array);
                    strBodyPart = "Bottom";
                    break;
                case "Shoes":
                    setUpAdapterData(strCategory.equals("Casual")
                            ? R.array.male_casuals_shoes_array
                            : R.array.male_formals_shoes_array);
                    strBodyPart = "Shoes";
                    break;
            }
        }

        if (spinnerTop != null) {
            spinnerTop.setAdapter(adapterTop);
            spinnerTop.setOnItemSelectedListener(BodyActivity.this);
        }
    }

    private void setUpAdapterData(int arrData) {
        adapterTop = ArrayAdapter.createFromResource(BodyActivity.this, arrData,
                R.layout.simple_spinner_text_view);
        adapterTop.setDropDownViewResource(R.layout.simple_spinner_text_view);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String strFormattedName = ((String) parent.getItemAtPosition(position))
                .replaceAll("\\s", "")
                .toLowerCase();

        int drawableId = getResources().getIdentifier(strFormattedName,
                "drawable", getPackageName());
        imgViewTop.setBackground(getResources().getDrawable(drawableId));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }
}
