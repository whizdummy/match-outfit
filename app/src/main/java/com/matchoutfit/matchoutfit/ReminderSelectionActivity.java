package com.matchoutfit.matchoutfit;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReminderSelectionActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener {

    private EditText txtReminder;
    private Calendar calendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private Spinner spinnerOutfit;
    private ImageView imgTop, imgBottom, imgShoes;
    private ArrayList<String> arrTop, arrBottom, arrShoes;
    private Date dateReminder;

    private void formatDate() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateReminder = calendar.getTime();

        txtReminder.setText(sdf.format(calendar.getTime()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_selection);
        setUpData();
        setUpSpinnerData();
        txtReminderOnClick();
    }

    private void setUpSpinnerData() {
        fetchOutfits();
    }

    private void fetchOutfits() {
        ParseQuery<ParseObject> queryOutfits = ParseQuery.getQuery("MatchOutfit");

        queryOutfits.whereEqualTo("User", ParseUser.getCurrentUser());

        queryOutfits.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null) {
                    ArrayList<CharSequence> arrOutfits = new ArrayList<>();

                    for(ParseObject objOutfit: objects) {
                        arrOutfits.add(objOutfit.getString("MatchName"));
                        arrTop.add(objOutfit.getString("Top"));
                        arrBottom.add(objOutfit.getString("Bottom"));
                        arrShoes.add(objOutfit.getString("Shoes"));
                    }

                    ArrayAdapter<CharSequence> arrAdapter = new
                            ArrayAdapter<>(ReminderSelectionActivity.this,
                            R.layout.simple_spinner_text_view,
                            arrOutfits);
                    arrAdapter.setDropDownViewResource(R.layout.simple_spinner_text_view);

                    spinnerOutfit.setAdapter(arrAdapter);
                    spinnerOutfit.setOnItemSelectedListener(ReminderSelectionActivity.this);
                } else {
                    Toast.makeText(ReminderSelectionActivity.this, "Error fetching outfit details.",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    private void txtReminderOnClick() {
        txtReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ReminderSelectionActivity.this, date,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void setUpData() {
        txtReminder = (EditText) findViewById(R.id.txt_reminder);
        spinnerOutfit = (Spinner) findViewById(R.id.spinner_outfit);
        imgTop = (ImageView) findViewById(R.id.img_top);
        imgBottom = (ImageView) findViewById(R.id.img_bottom);
        imgShoes = (ImageView) findViewById(R.id.img_shoes);
        arrTop = new ArrayList<>();
        arrBottom = new ArrayList<>();
        arrShoes = new ArrayList<>();

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                formatDate();
            }
        };

        setTitle("Add Outfit Reminder");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void addReminderOnClick(View view) throws ParseException {
        if(!txtReminder.getText().toString().trim().equals("")) {
            ParseObject objReminder = new ParseObject("Reminder");

            objReminder.put("MatchOutfit", ParseObject.createWithoutData("MatchOutfit",
                    getOutfitObjectId(spinnerOutfit
                            .getSelectedItem()
                            .toString())));
            objReminder.put("reminderDate", dateReminder);

            objReminder.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null) {
                        finish();

                        Toast.makeText(ReminderSelectionActivity.this, "Reminder saved successfully.",
                                Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(ReminderSelectionActivity.this, "Error saving reminder." +
                                        " Please try again.",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });
        } else {
            Toast.makeText(ReminderSelectionActivity.this, "Empty reminder date.",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private String getOutfitObjectId(String strOutfitName) throws ParseException {
        ParseQuery<ParseObject> queryOutfit = ParseQuery.getQuery("MatchOutfit");
        queryOutfit.whereEqualTo("User", ParseUser.getCurrentUser());
        queryOutfit.whereEqualTo("MatchName", strOutfitName);

        return queryOutfit.getFirst().getObjectId();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        imgTop.setBackground(getResources().getDrawable(getResources()
                .getIdentifier(arrTop.get(position)
                .replaceAll("\\s", "")
                .toLowerCase(),
                "drawable", getPackageName())));
        imgBottom.setBackground(getResources().getDrawable(getResources()
                .getIdentifier(arrBottom.get(position)
                                .replaceAll("\\s", "")
                                .toLowerCase(),
                        "drawable", getPackageName())));
        imgShoes.setBackground(getResources().getDrawable(getResources()
                .getIdentifier(arrShoes.get(position)
                                .replaceAll("\\s", "")
                                .toLowerCase(),
                        "drawable", getPackageName())));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
