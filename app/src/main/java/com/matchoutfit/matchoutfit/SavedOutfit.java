package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class SavedOutfit extends AppCompatActivity {

    private ListView listViewSavedOutfits;
    private ArrayAdapter<String> listAdapter;

    @Override
    protected void onResume() {
        super.onResume();

        setUpListViewData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_outfit);
        setUpData();
    }

    private void setUpData() {
        listViewSavedOutfits = (ListView) findViewById(R.id.list_view_saved_outfits);

        setTitle("Saved Outfits");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpListViewData() {
        getSavedOutfits();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getSavedOutfits() {
        final ArrayList<String> arrSavedOutfits = new ArrayList<>();
        final ArrayList<String> arrSavedOutfitIds = new ArrayList<>();
        ParseQuery<ParseObject> querySavedOutfits = ParseQuery.getQuery("MatchOutfit");

        querySavedOutfits.whereEqualTo("User", ParseUser.getCurrentUser());

        querySavedOutfits.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null) {
                    for (ParseObject object: objects) {
                        arrSavedOutfits.add(object.getString("MatchName"));
                        arrSavedOutfitIds.add(object.getObjectId());
                    }

                    listAdapter = new ArrayAdapter<String>(SavedOutfit.this,
                            R.layout.simple_spinner_text_view,
                            arrSavedOutfits);

                    listViewSavedOutfits.setAdapter(listAdapter);

                    listViewSavedOutfits.setOnItemClickListener(
                            new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position,
                                                long id) {
                            Intent intentOutfit =
                                    new Intent(SavedOutfit.this, SavedOutfitViewActivity.class);

                            intentOutfit.putExtra("OUTFIT_ID",
                                    arrSavedOutfitIds.get(position));
                            intentOutfit.putExtra("OUTFIT_NAME",
                                    arrSavedOutfits.get(position));

                            startActivity(intentOutfit);
                        }
                    });
                } else {
                    Toast.makeText(SavedOutfit.this, "Error fetching outfit details.",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }
}
