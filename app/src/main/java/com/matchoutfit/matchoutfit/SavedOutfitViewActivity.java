package com.matchoutfit.matchoutfit;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.SendButton;
import com.facebook.share.widget.ShareButton;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class SavedOutfitViewActivity extends AppCompatActivity {

    private ImageView imgTop, imgBottom, imgShoes;
    private String strOutfitId, strOutfitName, strTop, strBottom, strShoes;
    private ShareButton btnShareOutfit;
    private SendButton btnSendOutfit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_saved_outfit_view);
        setUpData();
        setUpDataOnView();
    }

    private void setUpDataOnView() {
        getOutfitViaId();
    }

    private void getOutfitViaId() {
        ParseQuery<ParseObject> queryOutfit = ParseQuery.getQuery("MatchOutfit");
        queryOutfit.whereEqualTo("objectId", strOutfitId);

        queryOutfit.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if(e == null) {
                    strTop = object.getString("Top");
                    strBottom = object.getString("Bottom");
                    strShoes = object.getString("Shoes");

                    imgTop.setBackground(getResources()
                            .getDrawable(getResources().getIdentifier(strTop
                            .replaceAll("\\s", "")
                            .toLowerCase(), "drawable", getPackageName())));

                    imgBottom.setBackground(getResources()
                            .getDrawable(getResources().getIdentifier(strBottom
                                    .replaceAll("\\s", "")
                                    .toLowerCase(), "drawable", getPackageName())));

                    imgShoes.setBackground(getResources()
                            .getDrawable(getResources().getIdentifier(strShoes
                                    .replaceAll("\\s", "")
                                    .toLowerCase(), "drawable", getPackageName())));

                    SharePhoto photoTop = new SharePhoto.Builder().setBitmap(BitmapFactory
                            .decodeResource(getResources(), getResources().getIdentifier(strTop
                                    .replaceAll("\\s", "")
                                    .toLowerCase(), "drawable", getPackageName()))).build();
                    SharePhoto photoBottom = new SharePhoto.Builder().setBitmap(BitmapFactory
                            .decodeResource(getResources(), getResources().getIdentifier(strBottom
                                    .replaceAll("\\s", "")
                                    .toLowerCase(), "drawable", getPackageName()))).build();
                    SharePhoto photoShoes = new SharePhoto.Builder().setBitmap(BitmapFactory
                            .decodeResource(getResources(), getResources().getIdentifier(strShoes
                                    .replaceAll("\\s", "")
                                    .toLowerCase(), "drawable", getPackageName()))).build();

                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photoTop)
                            .addPhoto(photoBottom)
                            .addPhoto(photoShoes)
                            .build();

                    btnShareOutfit.setShareContent(content);
                    btnSendOutfit.setShareContent(content);
                } else {
                    Toast.makeText(SavedOutfitViewActivity.this, "An error occurred. " +
                            "Please try again",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpData() {
        imgTop = (ImageView) findViewById(R.id.img_top);
        imgBottom = (ImageView) findViewById(R.id.img_bottom);
        imgShoes = (ImageView) findViewById(R.id.img_shoes);
        btnShareOutfit = (ShareButton) findViewById(R.id.btn_share);
        btnSendOutfit = (SendButton) findViewById(R.id.btn_send);

        Bundle extraData = getIntent().getExtras();

        if(extraData != null) {
            strOutfitId = extraData.getString("OUTFIT_ID");
            strOutfitName = extraData.getString("OUTFIT_NAME");
        }

        setTitle(strOutfitName);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void deleteOutfitOnClick(View view) {
        ParseQuery<ParseObject> queryOutfit = new ParseQuery<>("MatchOutfit");

        queryOutfit.whereEqualTo("objectId", strOutfitId);

        queryOutfit.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if(e == null) {
                    object.deleteInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e == null) {
                                finish();
                            } else {
                                Toast.makeText(SavedOutfitViewActivity.this,
                                        "Error while deleting outfit. Please try again",
                                        Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(SavedOutfitViewActivity.this, "Error fetching outfit details.",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }
}
