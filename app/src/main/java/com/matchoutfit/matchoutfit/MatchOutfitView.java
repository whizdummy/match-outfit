package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class MatchOutfitView extends AppCompatActivity {

    private String[] strArrBodyParts = new String[3];   // top, bottom, shoes
    private ImageView[] arrImages = new ImageView[3];
    private int[] arrDrawableId = new int[3];
    private String strCategory, strGender;
    private EditText txtOutfitName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_outfit_view);
        setUpData();
        setUpOutfitViews();
    }

    private void setUpData() {
        Bundle bundleData = getIntent().getExtras();

        if(bundleData != null) {
            strArrBodyParts[0] = bundleData.getString("TOP");
            strArrBodyParts[1] = bundleData.getString("BOTTOM");
            strArrBodyParts[2] = bundleData.getString("SHOES");
            strCategory = bundleData.getString("CATEGORY");
            strGender = bundleData.getString("GENDER");
        }

        txtOutfitName = (EditText) findViewById(R.id.txt_outfit_name);

        setTitle("Outfit Summary");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void setUpOutfitViews() {
        arrImages[0] = (ImageView) findViewById(R.id.img_view_top);
        arrImages[1] = (ImageView) findViewById(R.id.img_view_bottom);
        arrImages[2] = (ImageView) findViewById(R.id.img_view_shoes);

        for (int i = 0; i < 3; i++) {
            arrDrawableId[i] = getResources().getIdentifier(strArrBodyParts[i]
                    .replaceAll("\\s", "")
                    .toLowerCase(), "drawable", getPackageName());
        }

        for(int i = 0; i < 3; i++) {
            arrImages[i].setBackground(getResources().getDrawable(arrDrawableId[i]));
        }
    }

    public void saveOnClick(View view) {
        if(!txtOutfitName.getText().toString().trim().equals("")) {
              saveOutfit();
        } else {
            Toast.makeText(MatchOutfitView.this, "No outfit name!", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveOutfit() {
        ParseObject outfitObject = new ParseObject("MatchOutfit");

        outfitObject.put("User", ParseUser.getCurrentUser());
        outfitObject.put("Gender", strGender);
        outfitObject.put("Category", strCategory);
        outfitObject.put("Top", strArrBodyParts[0]);
        outfitObject.put("Bottom", strArrBodyParts[1]);
        outfitObject.put("Shoes", strArrBodyParts[2]);
        outfitObject.put("MatchName", txtOutfitName.getText().toString().trim());

        outfitObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    Intent intentMenu = new Intent(MatchOutfitView.this, MenuActivity.class);

                    startActivity(intentMenu);
                } else {
                    Toast.makeText(MatchOutfitView.this, "Error saving outfit. Please try again.",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(MatchOutfitView.this, "Cancelled.", Toast.LENGTH_SHORT).show();

        strArrBodyParts = null;

        super.onBackPressed();
    }

    public void cancelOnClick(View view) {
        onBackPressed();
    }
}
