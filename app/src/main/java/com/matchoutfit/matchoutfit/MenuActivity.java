package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setUpData();
    }

    private void setUpData() {
        setTitle("Menu");
    }

    public void matchOutfitOnClick(View view) {
        Intent intentBody = new Intent(MenuActivity.this, MatchOutfitActivity.class);

        startActivity(intentBody);
    }

    public void savedOutfitOnClick(View view) {
        Intent intentSavedOutfit = new Intent(MenuActivity.this, SavedOutfit.class);

        startActivity(intentSavedOutfit);
    }

    public void reminderOutfitOnClick(View view) {
        Intent intentReminder = new Intent(MenuActivity.this, ReminderActivity.class);

        startActivity(intentReminder);
    }

    public void logOutOnClick(View view) {
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    Intent intentLogin = new Intent(MenuActivity.this, LoginActivity.class);

                    finish();

                    Toast.makeText(MenuActivity.this, "Successfully logged out.",
                            Toast.LENGTH_SHORT)
                            .show();

                    startActivity(intentLogin);
                } else {
                    Toast.makeText(MenuActivity.this, "Error logging out.", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }
}
