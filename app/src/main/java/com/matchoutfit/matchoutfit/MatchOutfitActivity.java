package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class MatchOutfitActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String strGender, strCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_outfit);
        setUpView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpView() {
        Spinner spinnerCategory = (Spinner) findViewById(R.id.spinner_categories);

        ArrayAdapter<CharSequence> adapterCategory = ArrayAdapter.createFromResource(this,
                R.array.categories_array, R.layout.simple_spinner_text_view);

        adapterCategory.setDropDownViewResource(R.layout.simple_spinner_text_view);

        if (spinnerCategory != null) {
            spinnerCategory.setAdapter(adapterCategory);

            spinnerCategory.setOnItemSelectedListener(this);
        }

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void bodyOnClick(View view) {

        if(strGender != null && strCategory != null) {
            Intent intentMenu = new Intent(MatchOutfitActivity.this, MatchOutfitBodyActivity.class);

            intentMenu.putExtra("GENDER", strGender);
            intentMenu.putExtra("CATEGORY", strCategory);

            startActivity(intentMenu);
        } else if(strGender == null) {
            Toast.makeText(MatchOutfitActivity.this, "No gender selected!", Toast.LENGTH_SHORT)
                    .show();
        } else if(strCategory == null) {
            Toast.makeText(MatchOutfitActivity.this, "No category selected!", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public void genderOnClick(View view) {
        boolean isChecked = ((RadioButton) view).isChecked();

        if(isChecked) {
            switch (view.getId()) {
                case R.id.radio_female:
                    strGender = "Female";
                    break;
                case R.id.radio_male:
                    strGender = "Male";
                    break;
            }
        } else {
            Toast.makeText(MatchOutfitActivity.this, "Radio button unchecked", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        strCategory = (String) parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }
}
