package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class ReminderActivity extends AppCompatActivity {

    private ListView listViewSavedReminders;
    private ArrayAdapter<String> listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        setUpData();
    }

    private void setUpData() {
        listViewSavedReminders = (ListView) findViewById(R.id.list_view_saved_reminders);

        setTitle("Outfit Reminders");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void reminderOnClick(View view) {
        Intent intentReminderSelection = new Intent(this, ReminderSelectionActivity.class);

        startActivity(intentReminderSelection);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpListViewData();
    }

    private void setUpListViewData() {
        getSavedReminders();
    }

    private void getSavedReminders() {
        final ArrayList<String> arrSavedReminders = new ArrayList<>();
        final ArrayList<String> arrSavedOutfits = new ArrayList<>();

        ParseQuery<ParseObject> querySavedOutfits = ParseQuery.getQuery("MatchOutfit");
        querySavedOutfits.whereEqualTo("User", ParseUser.getCurrentUser());

        ParseQuery<ParseObject> queryReminders = ParseQuery.getQuery("Reminder");
        queryReminders.whereMatchesQuery("MatchOutfit", querySavedOutfits);

        queryReminders.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null) {
                    for(ParseObject object: objects) {
                        arrSavedReminders.add(object.getDate("reminderDate").toString());
                    }

                    listAdapter = new ArrayAdapter<String>(ReminderActivity.this,
                            R.layout.simple_spinner_text_view,
                            arrSavedReminders);

                    listViewSavedReminders.setAdapter(listAdapter);
                } else {
                    Toast.makeText(ReminderActivity.this, e.getMessage(), Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }
}
