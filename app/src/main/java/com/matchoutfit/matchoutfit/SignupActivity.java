package com.matchoutfit.matchoutfit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignupActivity extends AppCompatActivity {

    private EditText txtName, txtEmail, txtUsername, txtPassword, txtRePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setUpData();
    }

    private void setUpData() {
        txtName = (EditText) findViewById(R.id.txt_name);
        txtEmail = (EditText) findViewById(R.id.txt_email);
        txtUsername = (EditText) findViewById(R.id.txt_username);
        txtPassword = (EditText) findViewById(R.id.txt_password);
        txtRePassword = (EditText) findViewById(R.id.txt_retype_password);

        setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int intItemId = item.getItemId();

        if(intItemId == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void signUpOnClick(View view) {
        saveCreation();
    }

    private void saveCreation() {
        ParseUser user = new ParseUser();

        user.put("name", txtName.getText().toString());
        user.setEmail(txtEmail.getText().toString());
        user.setUsername(txtUsername.getText().toString());
        if(txtPassword.getText().toString().equals(txtRePassword.getText().toString())) {
            user.setPassword(txtPassword.getText().toString());

            signUp(user);
        } else {
            Toast.makeText(SignupActivity.this, "Passwords do not match.",
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void signUp(ParseUser user) {
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if(e != null) {
                    Toast.makeText(SignupActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SignupActivity.this, "Signed up successfuly!",
                            Toast.LENGTH_SHORT)
                            .show();

                    finish();
                }
            }
        });
    }
}
