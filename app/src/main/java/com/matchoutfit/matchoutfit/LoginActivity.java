package com.matchoutfit.matchoutfit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends AppCompatActivity {

    private EditText txtUsername, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(ParseUser.getCurrentUser() != null) {
            gotoLogin();
        } else {
            setContentView(R.layout.activity_login);
        }
        setUpData();
    }

    private void setUpData() {
        txtUsername = (EditText) findViewById(R.id.txt_username);
        txtPassword = (EditText) findViewById(R.id.txt_password);

        setTitle("Match Outfit Login");
    }

    public void logInOnClick(View view) {
        ParseUser.logInInBackground(txtUsername.getText().toString(), txtPassword.getText().toString(),
                new LogInCallback() {

                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if(user != null) {
                            gotoLogin();
                        } else {
                            Toast.makeText(LoginActivity.this, e.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void gotoLogin() {
        finish();

        Intent intentOutfitMenu = new Intent(LoginActivity.this, MenuActivity.class);

        startActivity(intentOutfitMenu);
    }

    public void signUpViewOnClick(View view) {
        Intent intentSignUp = new Intent(LoginActivity.this, SignupActivity.class);

        startActivity(intentSignUp);
    }
}
